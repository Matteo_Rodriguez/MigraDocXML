﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaGenerator
{

    /// <summary>
    /// The xml schema was becoming pretty unmanageable to maintain just by xml, I added this project to make the process a bit easier
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            MigraDocXMLSchema schema = new MigraDocXMLSchema();
            ZXingSchemaAdditions zxingAdditions = new ZXingSchemaAdditions();

            Console.WriteLine("Would you like the schema to include the ZXing.NET extension? Y/N");
            bool zxing = (Console.ReadKey().Key == ConsoleKey.Y);

            if (zxing) zxingAdditions.RunBeforeBuild(schema);
            schema.Build();
            if (zxing) zxingAdditions.RunAfterBuild(schema);

            string schemaStr = new SchemaCompiler().Compile(schema);

            Console.WriteLine(schemaStr);

            Console.WriteLine("Enter save location:");
            string saveLocation = Console.ReadLine();

            try
            {
                System.IO.File.WriteAllText(saveLocation, schemaStr);
                Console.WriteLine("Save successful");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Failed to write file");
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
    }
}
