<?xml version="1.0" encoding="utf-8"?>
<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:noNamespaceSchemaLocation="https://gitlab.com/jamescoyle/MigraDocXML/raw/master/MigraDocXML/schema.xsd">

    <Style Target="Paragraph">
        <Setters Format.SpaceBefore="2mm" Format.SpaceAfter="2mm" Format.Font.Name="Calibri"/>
    </Style>

    <Style Target="Paragraph" Name="Title">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single"/>
    </Style>

    <Style Target="Paragraph" Name="SubTitle">
        <Setters Format.Font.Bold="true" Format.Font.Underline="Single" Format.SpaceBefore="8mm" Format.Font.Size="11"/>
    </Style>

    <Style Target="Paragraph" Name="Code">
        <Setters Format.Font.Name="Consolas" Format.SpaceBefore="0" Format.SpaceAfter="0" Format.Font.Size="8" Format.Shading.Color="LightGray"/>
    </Style>

    <Style Target="FormattedText" Name="Code">
        <Setters Font.Name="Consolas" Font.Size="8" Font.Bold="false" Font.Italic="false"/>
    </Style>

    <Section>
        <p Format.Alignment="Center" Style="Title" Format.Font.Size="18" Format.SpaceAfter="5mm">MigraDocXML Lesson 3.9 - Accessing The DOM</p>

        <p>As well as being able to access passed in object/file data, scripts can also access the properties of display object models to make use of their attributes. For example:</p>

        <p Style="Code">&lt;p&gt;```Page Size = {Section.PageSetup.PageWidth} x {Section.PageSetup.PageHeight}```&lt;/p&gt;</p>

        <p>Produces:</p>

        <p>Page Size = {Section.PageSetup.PageWidth} x {Section.PageSetup.PageHeight}</p>

        <p>At creation, each DOM element registers a variable for its name (as well as for any aliases). So for example you can access the attributes of a paragraph by using <i Style="Code">Paragraph</i> or <i Style="Code">p</i>.</p>

        <p>It's important to note that since these are accessed as variables, the usual variable scope rules apply, so you can only access the attributes of a table from within that table.</p>

        <p>Additionally, if you have nested elements, for example a PointList within a PointList, then using the <i Style="Code">list</i> variable will always access the inner-most PointList that contains the calling script.</p>

        <p>A good example of when this can be incredibly useful is when you want to define a table that runs exactly up to both the left and right margins of the page. Since table widths are defined by the sum of their column widths, doing this manually can be a massive pain, and even more so if a new column needs to be added.</p>

        <p>By using <i Style="Code">Section.PageSetup.ContentWidth</i>, we can define our list of rows by their percentage widths.</p>

        <p Style="Code">
            &lt;Var width=&quot;Section.PageSetup.ContentWidth&quot;/&gt;
            &lt;Table Borders.Color=&quot;Black&quot;&gt;
            {Space(4)}&lt;Column Width=&quot;```{width * 0.4}```&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;```{width * 0.3}```&quot;/&gt;
            {Space(4)}&lt;Column Width=&quot;```{width * 0.3}```&quot;/&gt;

            {Space(4)}&lt;Row C0=&quot;Full&quot; C1=&quot;Width&quot; C2=&quot;Table&quot;/&gt;
            &lt;/Table&gt;
            
        </p>

        <p>Produces:</p>

        <Var width="Section.PageSetup.ContentWidth"/>
        <Table Borders.Color="Black">
            <Column Width="{width * 0.4}"/>
            <Column Width="{width * 0.3}"/>
            <Column Width="{width * 0.3}"/>

            <Row C0="Full" C1="Width" C2="Table"/>
        </Table>

        <p>Now all we need to do to keep the table at full width is to make sure the column width multipliers sum to 1.</p>
    </Section>
</Document>