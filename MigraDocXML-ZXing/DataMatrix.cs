﻿using MigraDocXML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    public class DataMatrix : Barcode
    {

        private ZXing.Datamatrix.DatamatrixEncodingOptions _options;


        public DataMatrix()
            : base()
        {
            _options = new ZXing.Datamatrix.DatamatrixEncodingOptions();
            _writer.Options = _options;
            _writer.Format = ZXing.BarcodeFormat.DATA_MATRIX;
        }


		public override void SetTextValue(string value)
		{
			if (GS1Format)
			{
				List<KeyValuePair<string, string>> splits = null;
				//If we can't even succeed in splitting out the text into a GS1 compliant format, then treat it as not GS1
				try
				{
					splits = GS1.SplitReadableText(value);
				}
				catch
				{
					base.SetTextValue(value);
					return;
				}

				string gs1Text = GS1.BuildGS1Barcode(splits, (char)29);
				base.SetTextValue(gs1Text);
			}
			else
				base.SetTextValue(value);
		}


		public int? DefaultEncodation { get => _options.DefaultEncodation; set => _options.DefaultEncodation = value; }

        public int MaxWidth { get => _options.MaxSize?.Width ?? -1; set => _options.MaxSize = new ZXing.Dimension(value, _options.MaxSize?.Height ?? value); }

        public int MaxHeight { get => _options.MaxSize?.Height ?? -1; set => _options.MaxSize = new ZXing.Dimension(_options.MaxSize?.Width ?? value, value); }

        public int MinWidth { get => _options.MinSize?.Width ?? -1; set => _options.MinSize = new ZXing.Dimension(value, _options.MinSize?.Height ?? value); }

        public int MinHeight { get => _options.MinSize?.Height ?? -1; set => _options.MinSize = new ZXing.Dimension(_options.MinSize?.Width ?? value, value); }

        public string SymbolShape
        {
            get => _options.SymbolShape.ToString();
            set => _options.SymbolShape = Parse.Enum<ZXing.Datamatrix.Encoder.SymbolShapeHint>(value);
        }
    }
}
