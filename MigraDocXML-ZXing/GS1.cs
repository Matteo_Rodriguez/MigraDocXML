﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MigraDocXML_ZXing
{
    public static class GS1
    {
        /// <summary>
        /// Takes text such as (01)12345(21)019238 and splits it out into key value pairs of AI codes and data
        /// </summary>
        /// <param name="gs1Text">The text to interpret</param>
        /// <returns></returns>
        internal static List<KeyValuePair<string, string>> SplitReadableText(string gs1Text)
        {
            //Validate that the text contains at least one AI wrapped in brackets
            if (!gs1Text.StartsWith("(") || !gs1Text.Contains(')'))
                throw new Exception("Invalid GS1 barcode text");
			gs1Text = gs1Text.Substring(1);
            
            //Split out the barcode text into a list of AIs associated with data
            var gs1Data = new List<KeyValuePair<string, string>>();
            foreach(var gs1Split in gs1Text.Split('('))
            {
                var splitSplit = gs1Split.Split(')');
                if (splitSplit.Length != 2)
                    throw new Exception("Invalid GS1 barcode text");
				
                var aiCode = splitSplit[0];
                if (gs1Data.Any(x => x.Key == aiCode))
                    throw new Exception("GS1 barcode contains the same AI multiple times");
                var data = splitSplit[1];
                gs1Data.Add(new KeyValuePair<string, string>(aiCode, data));
            }

			return gs1Data;
        }


		/// <summary>
		/// Takes in a list of key value pairs representing the data in a GS1 barcode and returns a string for the actual barcode content
		/// </summary>
		/// <param name="ais"></param>
		/// <param name="fnc1"></param>
		/// <returns></returns>
		internal static string BuildGS1Barcode(List<KeyValuePair<string, string>> ais, char fnc1)
		{
			AI prevAiObj = null;
			StringBuilder stb = new StringBuilder();
			for(int i = 0; i < ais.Count; i++)
			{
				var ai = ais[i];
				AI aiObj = AIs.FirstOrDefault(x => x.CodeMatch(ai.Key));
				if (aiObj == null)
					throw new Exception("Unrecognised AI code: " + ai.Key);
				if (aiObj.VariableCodeEnd)
					aiObj = new AI().SetCode(ai.Key, variableEnd: true).SetLength(aiObj.MinLength, aiObj.MaxLength);

				if (ai.Value.Length < aiObj.MinLength || ai.Value.Length > aiObj.MaxLength)
					throw new Exception("Invalid data length for AI: " + aiObj.Code);

				if (i == 0 || prevAiObj?.IsVariableLength == true)
					stb.Append(fnc1);
				stb.Append(aiObj.Code);
				stb.Append(ai.Value);
				prevAiObj = aiObj;
			}
			return stb.ToString();
		}

		
        internal static IEnumerable<AI> AIs { get; private set; }

        static GS1()
        {
            //Full AI list taken from here:
            //https://www.gs1.org/sites/default/files/docs/barcodes/GS1_General_Specifications.pdf pages 135 - 139
            AIs = new List<AI>()
            {
                new AI().SetCode("00").SetLength(18),       //Serial Shipping Container Code (SSCC)
                new AI().SetCode("01").SetLength(14),       //Global Trade Item Number (GTIN)
                new AI().SetCode("02").SetLength(14),       //GTIN of contained trade items
                new AI().SetCode("10").SetLength(1, 20),    //Batch or lot number
                new AI().SetCode("11").SetLength(6),        //Production date (YYMMDD)
                new AI().SetCode("12").SetLength(6),        //Due date (YYMMDD)
                new AI().SetCode("13").SetLength(6),        //Packaging date (YYMMDD)
                new AI().SetCode("15").SetLength(6),        //Best before date (YYMMDD)
                new AI().SetCode("16").SetLength(6),        //Sell by date (YYMMDD)
                new AI().SetCode("17").SetLength(6),        //Expiration date (YYMMDD)
                new AI().SetCode("20").SetLength(2),        //Internal product variant
                new AI().SetCode("21").SetLength(1, 20),    //Serial number
                new AI().SetCode("22").SetLength(1, 20),    //Consumer product variant
                new AI().SetCode("240").SetLength(1, 30),   //Additional product identification assigned by the manufacturer
                new AI().SetCode("241").SetLength(1, 30),   //Customer part number
                new AI().SetCode("242").SetLength(1, 6),    //Made-to-Order variation number
                new AI().SetCode("243").SetLength(1, 20),   //Packaging component number
                new AI().SetCode("250").SetLength(1, 30),   //Secondary serial number
                new AI().SetCode("251").SetLength(1, 30),   //Reference to source entity
                new AI().SetCode("253").SetLength(13, 30),  //Global Document Type Identifier (GDTI)
                new AI().SetCode("254").SetLength(1, 20),   //GLN extension component
                new AI().SetCode("255").SetLength(13, 25),  //Global Coupon Number (GCN)
                new AI().SetCode("30").SetLength(1, 8),     //Variable count of items (variable measure trade item)
                new AI().SetCode("310", variableEnd: true).SetLength(6),    //Net weight, kilograms (variable measure trade item)
                new AI().SetCode("311", variableEnd: true).SetLength(6),    //Length or first dimension, meters (variable measure trade item)
                new AI().SetCode("312", variableEnd: true).SetLength(6),    //Width, diameter, or second dimension, meters (variable measure trade item)
                new AI().SetCode("313", variableEnd: true).SetLength(6),    //Depth, thickness, height, or third dimension, meters (variable measure trade item)
                new AI().SetCode("314", variableEnd: true).SetLength(6),    //Area, square metres (variable measure trade item)
                new AI().SetCode("315", variableEnd: true).SetLength(6),    //Net volume, litres (variable measure trade item)
                new AI().SetCode("316", variableEnd: true).SetLength(6),    //Net volume, cubic metres (variable measure trade item)
                new AI().SetCode("320", variableEnd: true).SetLength(6),    //Net weight, pounds (variable measure trade item)
                new AI().SetCode("321", variableEnd: true).SetLength(6),    //Length or first dimension, inches (variable measure trade item)
                new AI().SetCode("322", variableEnd: true).SetLength(6),    //Length or first dimension, feet (variable measure trade item)
                new AI().SetCode("323", variableEnd: true).SetLength(6),    //Length or first dimension, yards (variable measure trade item)
                new AI().SetCode("324", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, inches (variable measure trade item)
                new AI().SetCode("325", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, feet (variable measure trade item)
                new AI().SetCode("326", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, yards (variable measure trade item)
                new AI().SetCode("327", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, inches (variable measure trade item)
                new AI().SetCode("328", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, feet (variable measure trade item)
                new AI().SetCode("329", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, yards (variable measure trade item)
                new AI().SetCode("330", variableEnd: true).SetLength(6),	//Logistic weight, kilograms
                new AI().SetCode("331", variableEnd: true).SetLength(6),	//Length or first dimension, metres
                new AI().SetCode("332", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, metres
                new AI().SetCode("333", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, metres
                new AI().SetCode("334", variableEnd: true).SetLength(6),	//Area, square metres
                new AI().SetCode("335", variableEnd: true).SetLength(6),	//Logistic volume, litres
                new AI().SetCode("336", variableEnd: true).SetLength(6),	//Logistic volume, cubic metres
                new AI().SetCode("337", variableEnd: true).SetLength(6),	//Kilograms per square metre
                new AI().SetCode("340", variableEnd: true).SetLength(6),	//Logistic weight, pounds
                new AI().SetCode("341", variableEnd: true).SetLength(6),	//Length or first dimension, inches
                new AI().SetCode("342", variableEnd: true).SetLength(6),	//Length or first dimension, feet
                new AI().SetCode("343", variableEnd: true).SetLength(6),	//Length or first dimension, yards
                new AI().SetCode("344", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, inches
                new AI().SetCode("345", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, feet
                new AI().SetCode("346", variableEnd: true).SetLength(6),	//Width, diameter, or second dimension, yard
                new AI().SetCode("347", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, inches
                new AI().SetCode("348", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, feet
                new AI().SetCode("349", variableEnd: true).SetLength(6),	//Depth, thickness, height, or third dimension, yards
                new AI().SetCode("350", variableEnd: true).SetLength(6),	//Area, square inches (variable measure trade item)
                new AI().SetCode("351", variableEnd: true).SetLength(6),	//Area, square feet (variable measure trade item)
                new AI().SetCode("352", variableEnd: true).SetLength(6),	//Area, square yards (variable measure trade item)
                new AI().SetCode("353", variableEnd: true).SetLength(6),	//Area, square inches
                new AI().SetCode("354", variableEnd: true).SetLength(6),	//Area, square feet
                new AI().SetCode("355", variableEnd: true).SetLength(6),	//Area, square yards
                new AI().SetCode("356", variableEnd: true).SetLength(6),	//Net weight, troy ounces (variable measure trade item)
                new AI().SetCode("357", variableEnd: true).SetLength(6),	//Net weight (or volume), ounces (variable measure trade item)
                new AI().SetCode("360", variableEnd: true).SetLength(6),	//Net volume, quarts (variable measure trade item)
                new AI().SetCode("361", variableEnd: true).SetLength(6),	//Net volume, gallons U.S. (variable measure trade item)
                new AI().SetCode("362", variableEnd: true).SetLength(6),	//Logistic volume, quarts
                new AI().SetCode("363", variableEnd: true).SetLength(6),	//Logistic volume, gallons U.S.
                new AI().SetCode("364", variableEnd: true).SetLength(6),	//Net volume, cubic inches (variable measure trade item)
                new AI().SetCode("365", variableEnd: true).SetLength(6),	//Net volume, cubic feet (variable measure trade item)
                new AI().SetCode("366", variableEnd: true).SetLength(6),	//Net volume, cubic yards (variable measure trade item)
                new AI().SetCode("367", variableEnd: true).SetLength(6),	//Logistic volume, cubic inches
                new AI().SetCode("368", variableEnd: true).SetLength(6),	//Logistic volume, cubic feet
                new AI().SetCode("369", variableEnd: true).SetLength(6),	//Logistic volume, cubic yards
                new AI().SetCode("37").SetLength(1, 8),						//Count of trade items or trade item pieces contained in a logistic unit
                new AI().SetCode("390", variableEnd: true).SetLength(1, 15),	//Applicable amount payable or Coupon value, local currency
                new AI().SetCode("391", variableEnd: true).SetLength(3, 18),	//Applicable amount payable with ISO currency code
                new AI().SetCode("392", variableEnd: true).SetLength(1, 15),	//Applicable amount payable, single monetary area (variable measure trade item)
                new AI().SetCode("393", variableEnd: true).SetLength(3, 18),	//Applicable amount payable with ISO currency code (variable measure trade item)
                new AI().SetCode("394", variableEnd: true).SetLength(4),	//Percentage discount of a coupon
                new AI().SetCode("400").SetLength(1, 30),	//Customer's purchase order number
                new AI().SetCode("401").SetLength(1, 30),	//Global Identification Number for Consignment (GINC)
                new AI().SetCode("402").SetLength(17),		//Global Shipment Identification Number (GSIN)
                new AI().SetCode("403").SetLength(1, 30),	//Routing code
                new AI().SetCode("410").SetLength(13),		//Ship to - Deliver to Global Location Number
                new AI().SetCode("411").SetLength(13),		//Bill to - Invoice to Global Location Number
                new AI().SetCode("412").SetLength(13),		//Purchased from Global Location Number
                new AI().SetCode("413").SetLength(13),		//Ship for - Deliver for - Forward to Global Location Number
                new AI().SetCode("414").SetLength(13),		//Identification of a physical location - Global Location Number
                new AI().SetCode("415").SetLength(13),		//Global Location Number of the invoicing party
                new AI().SetCode("416").SetLength(13),		//GLN of the production or service location
                new AI().SetCode("420").SetLength(1, 20),	//Ship to - Deliver to postal code within a single postal authority
                new AI().SetCode("421").SetLength(3, 12),	//Ship to - Deliver to postal code with ISO country code
                new AI().SetCode("422").SetLength(3),		//Country of origin of a trade item
                new AI().SetCode("423").SetLength(3, 15),	//Country of initial processing
                new AI().SetCode("424").SetLength(3),		//Country of processing
                new AI().SetCode("425").SetLength(3, 15),	//Country of disassembly
                new AI().SetCode("426").SetLength(3),		//Country covering full process chain
                new AI().SetCode("427").SetLength(1, 3),	//Country subdivision of origin
                new AI().SetCode("7001").SetLength(13),		//NATO Stock Number (NSN)
                new AI().SetCode("7002").SetLength(1, 30),	//UN/ECE meat carcasses and cuts classification
                new AI().SetCode("7003").SetLength(10),		//Expiration date and time
                new AI().SetCode("7004").SetLength(1, 4),	//Active potency
                new AI().SetCode("7005").SetLength(1, 12),	//Catch area
                new AI().SetCode("7006").SetLength(6),		//First freeze date
                new AI().SetCode("7007").SetLength(6, 12),	//Harvest date
                new AI().SetCode("7008").SetLength(1, 3),	//Species for fishery purposes
                new AI().SetCode("7009").SetLength(1, 10),	//Fishing gear type
                new AI().SetCode("7010").SetLength(1, 2),	//Production method
                new AI().SetCode("7020").SetLength(1, 20),	//Refurbishment lot ID
                new AI().SetCode("7021").SetLength(1, 20),	//Functional status
                new AI().SetCode("7022").SetLength(1, 20),	//Revision status
                new AI().SetCode("7023").SetLength(1, 30),	//Global Individual Asset Identifier (GIAI) of an assembly
                new AI().SetCode("703", variableEnd: true).SetLength(3, 30),	//Number of processor with ISO Country Code
                new AI().SetCode("710").SetLength(1, 20),	//National Healthcare Reimbursement Number (NHRN) - Germany PZN
                new AI().SetCode("711").SetLength(1, 20),	//National Healthcare Reimbursement Number (NHRN) - Frace CIP
                new AI().SetCode("712").SetLength(1, 20),	//National Healthcare Reimbursement Number (NHRN) - Spain CN
                new AI().SetCode("713").SetLength(1, 20),	//National Healthcare Reimbursement Number (NHRN) - Brasil DRN
                new AI().SetCode("714").SetLength(1, 20),	//National Healthcare Reimbursement Number (NHRN) - Portugal AIM
                new AI().SetCode("723", variableEnd: true).SetLength(2, 30),	//Certification reference
                new AI().SetCode("8001").SetLength(14),		//Roll products (width, length, core diameter, direction, splices)
                new AI().SetCode("8002").SetLength(1, 20),	//Cellular mobile telephone identifier
                new AI().SetCode("8003").SetLength(14, 30),	//Global Returnable Asset Identifier (GRAI)
                new AI().SetCode("8004").SetLength(1, 30),	//Global Individual Asset Identifier (GIAI)
                new AI().SetCode("8005").SetLength(6),		//Price per unit of measure
                new AI().SetCode("8006").SetLength(18),		//Identification of an individual trade item place
                new AI().SetCode("8007").SetLength(1, 34),	//International Bank Account Number (IBAN)
                new AI().SetCode("8008").SetLength(8, 12),	//Date and time of production
                new AI().SetCode("8009").SetLength(1, 50),	//Optically Readable Sensor Indicator
                new AI().SetCode("8010").SetLength(1, 30),	//Component/Part Identifier (CPID)
                new AI().SetCode("8011").SetLength(1, 12),	//Component/Part Identifier serial number (CPID SERIAL)
                new AI().SetCode("8012").SetLength(1, 20),	//Software version
                new AI().SetCode("8013").SetLength(1, 30),	//Global Model Number (GMN)
                new AI().SetCode("8017").SetLength(18),		//Global Service Relation Number to identify the relationship between an organisation offering services and the provider of services
                new AI().SetCode("8018").SetLength(18),		//Global Service Relation Number to identify the relationship between an organisation offering services and the recipient of services
                new AI().SetCode("8019").SetLength(1, 10),	//Service Relation Instance Number (SRIN)
                new AI().SetCode("8020").SetLength(1, 25),	//Payment slip reference number
                new AI().SetCode("8026").SetLength(18),		//Identification of pieces of a trade item (ITIP) contained in a logistic unit
                new AI().SetCode("8110").SetLength(1, 70),	//Coupon code identification for use in North America
                new AI().SetCode("8111").SetLength(4),		//Loyalty points of a coupon
                new AI().SetCode("8112").SetLength(1, 70),	//Positive offer file coupon code identification for use in North America
                new AI().SetCode("8200").SetLength(1, 70),	//Extended Packaging URL
                new AI().SetCode("90").SetLength(1, 30),	//Information mutually agreed between trading partners
                new AI().SetCode("91").SetLength(1, 90),	//Company internal information
                new AI().SetCode("92").SetLength(1, 90),	//Company internal information
                new AI().SetCode("93").SetLength(1, 90),	//Company internal information
                new AI().SetCode("94").SetLength(1, 90),	//Company internal information
                new AI().SetCode("95").SetLength(1, 90),	//Company internal information
                new AI().SetCode("96").SetLength(1, 90),	//Company internal information
                new AI().SetCode("97").SetLength(1, 90),	//Company internal information
                new AI().SetCode("98").SetLength(1, 90),	//Company internal information
                new AI().SetCode("99").SetLength(1, 90)		//Company internal information
            };
        }



        internal class AI
        {
            public string Code { get; private set; }

            public bool VariableCodeEnd { get; private set; }

            public int MinLength { get; private set; }

            public int MaxLength { get; private set; }

            public bool IsVariableLength => MinLength != MaxLength;

            public AI SetCode(string code, bool variableEnd = false)
            {
                Code = code;
                VariableCodeEnd = variableEnd;
                return this;
            }

            public AI SetLength(int length)
            {
                MinLength = MaxLength = length;
                return this;
            }

            public AI SetLength(int minLength, int maxLength)
            {
                MinLength = minLength;
                MaxLength = maxLength;
                return this;
            }

            public bool CodeMatch(string aiCode)
            {
                if (VariableCodeEnd)
                {
                    if (aiCode.Length != Code.Length + 1)
                        return false;
                    return (aiCode.Substring(0, aiCode.Length - 1) == Code);
                }
                return aiCode == Code;
            }
        }
    }
}
