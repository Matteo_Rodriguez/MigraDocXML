﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class FieldTests
    {
        [Fact]
        public void PageFieldTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>Page <PageField/></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Page ", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.IsType<MigraDoc.DocumentObjectModel.Fields.PageField>(p.GetParagraphModel().Elements[1]);
            Assert.Equal(2, p.GetParagraphModel().Elements.Count);
        }

        [Fact]
        public void NumPagesFieldTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p><NumPagesField/> Pages</p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.IsType<MigraDoc.DocumentObjectModel.Fields.NumPagesField>(p.GetParagraphModel().Elements[0]);
            Assert.Equal(" Pages", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.Equal(2, p.GetParagraphModel().Elements.Count);
        }

        [Fact]
        public void DateFieldTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<p>Date <DateField Format=\"d MMM yyyy\"/></p>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            Paragraph p = section.Children.OfType<Paragraph>().First();

            Assert.Equal("Date ", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
            Assert.IsType<MigraDoc.DocumentObjectModel.Fields.DateField>(p.GetParagraphModel().Elements[1]);
            Assert.Equal("d MMM yyyy", p.GetParagraphModel().Elements.OfType<MigraDoc.DocumentObjectModel.Fields.DateField>().First().Format);
            Assert.Equal(2, p.GetParagraphModel().Elements.Count);
        }
    }
}
