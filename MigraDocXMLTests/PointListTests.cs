﻿using MigraDocXML;
using MigraDocXML.DOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace MigraDocXMLTests
{
    public class PointListTests
    {
        [Fact]
        public void TextTest()
        {
            string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<Document>" +
                    "<Section>" +
                        "<list NumberPosition=\"1.25cm\">" +
                            "<p>Item 1</p>" +
                            "<p>Item 2</p>" +
                            "<p>Item 3</p>" +
                        "</list>" +
                    "</Section>" +
                "</Document>";

            Document doc = new PdfXmlReader() { DesignText = code }.Run();
            Section section = doc.Children.OfType<Section>().First();
            PointList list = section.Children.OfType<PointList>().First();

            var pElements = list.Children.OfType<Paragraph>().Select(x => x.GetParagraphModel()).ToList();
            Assert.Equal(3, pElements.Count);

            foreach(var p in pElements)
            {
                Assert.Single(p.Elements);
                Assert.StartsWith("Item ", p.Elements.OfType<MigraDoc.DocumentObjectModel.Text>().First().Content);
                Assert.Equal(1.25, p.Format.ListInfo.NumberPosition.Value);
            }
        }


		[Fact]
		public void LoopInsideListTest()
		{
			string code = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				"<Document>" +
					"<Section>" +
						"<list NumberPosition=\"1.25cm\">" +
							"<ForEach Var=\"i\" In=\"[1, 2, 3]\">" +
								"<p>Item {i}</p>" +
							"</ForEach>" +
						"</list>" +
					"</Section>" +
				"</Document>";

			Document doc = new PdfXmlReader() { DesignText = code }.Run();
			Section section = doc.Children.OfType<Section>().First();
			PointList list = section.Children.OfType<PointList>().First();
			ForEach forEach = list.Children.OfType<ForEach>().First();

			var pElements = forEach.Children.OfType<Paragraph>().Select(x => x.GetParagraphModel()).ToList();
			Assert.Equal(3, pElements.Count);

			Assert.False(pElements[0].Format.ListInfo.ContinuePreviousList);
			Assert.True(pElements[1].Format.ListInfo.ContinuePreviousList);
			Assert.True(pElements[2].Format.ListInfo.ContinuePreviousList);
		}
    }
}
